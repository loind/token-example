/**
 * Created by baoan on 28/10/2015.
 */
var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    config = require('./config'),
    User = require('./models/User');


var port = process.env.PORT || 4242;
mongoose.connect(config.database);
app.set('superSecret', config.secret);

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(morgan('dev'));

app.get('/', function(req, res)
{
    res.send('Magic is happends on port ' + port);
});


app.get('/setup', function(req, res)
{
    var user = new User({
        username: 'admin',
        password: 'admin',
        admin: true
    });
    user.save(function(err)
    {
        if(err) throw err;
        console.log('User saved successfully');
        res.json({
            success: true
        });
    });
});



var apiRouter = express.Router();

apiRouter.use(function(req, res, next)
{
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if(token)
    {
        jwt.verify(token, app.get('superSecret'), function(err, decoded)
        {
            if(err)
            {
                res.json({
                    success:false,
                    message: 'Failed to authenticate token'
                });
            }
            else
            {
                req.decoded = decoded;
                next();
            }
        });
    }
    else
    {
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
});

apiRouter.get('/', function(req, res)
{
    res.json({
        message: 'Welcome to the coolest API on earth!'
    });
});

apiRouter.get('/users', function(req, res)
{
    User.find({}, function(err, users)
    {
        if(err) throw err;
        res.json(users);
    })
});

apiRouter.post('/authenticate', function(req, res)
{
    User.findOne({
        username: req.body.username
    }, function(err, user)
    {
        if(err) throw err;
        if(!user)
        {
            res.json({
                'success': false,
                'message': 'Authentication failed. User not found!'
            })
        }
        else if(user.password != req.body.password)
        {
            res.json({
                success: false,
                message: 'Authentication failed. Wrong password!'
            });
        }
        else
        {
            var token = jwt.sign(user, app.get('superSecret'), {
                expiresInMinutes: 1440
            });
            res.json({
                success: true,
                message: 'Enjoy your token',
                token: token
            })
        }
    });
});
app.use('/api', apiRouter);





app.listen(port, function()
{
    console.log('Server is running');
})
